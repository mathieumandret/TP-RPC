/**
 * TDM4 - Programme Serveur
 * Contient les procédure ls et read
 */


#include <rpc/rpc.h>
#include <dirent.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include <stdio.h>
#include <rpc/auth_unix.h>
#include "prog.h"
#include <string.h>
#include <strings.h>

void check_auth(struct svc_req* req)
{
	int auth_type;
	auth_type = req->rq_cred.oa_flavor;
	if (auth_type == AUTH_NULL) {
		printf("Aucune authentification utilisee\n");
		return;
	}
	// Recuperer les parametre d'identification
	struct authunix_parms* aup;
	aup = (struct authunix_parms*) req->rq_clntcred;
	printf("UID: %d\nGID: %d\nMACHINE: %s\n",
			aup->aup_uid,
			aup->aup_gid,
			aup->aup_machname
	);
	if (aup->aup_uid != getuid()) {
		printf("UID client != UID serveur");
	}
}

/**
 * Lit un répertoire de nom donné et renvoie une liste chaînée
 * des noms de tout son contenu
 */
res_file_list* rpc_ls_1_svc(char ** param, struct svc_req * req)
{
	check_auth(req);
	// Union statique a renvoyer
	static res_file_list* result;
	if (!result) result = malloc(sizeof(res_file_list));
	// Curseurs de parcours de la structure statique
	file_list* curs = NULL;
	file_list* prev_curs = NULL;
	// Dossier a lire
	DIR* directory;
	// Structure resultat de la lecture d'un dossier
	struct dirent* dir_content;
	// Ouverture du fichier
	if ((directory = opendir(param[0])) == NULL) {
		// En cas d'erreur, renseigner le numero
		// d'erreur et renvoyer l'union, qui contiendra void
		result->error = errno;
		return result;
	}
	// Parcours de toutes les entrees du dossier
	while((dir_content = readdir(directory))) {
		curs = malloc(sizeof(file_list*));
		curs->content = dir_content->d_name;
		curs->next = NULL;
		// Si le curseur pointe sur la premier element
		if (prev_curs == NULL) {
			// Placer son adresse dans le resultat
			result->res_file_list_u.list = curs;	
		} else {
			// Sinon ajouter la cellule courante
			// apres la precedente
			prev_curs->next = curs;
		}
		// Dans tous les cas, passer la cellule suivante
		prev_curs = curs;
	}
	// Il n'y a pas eu d'erreur
	result->error = 0;
	return result;
}

/**
 * Lit un fichier de nom donne et renvoie son
 * contenu dans une liste chaînée de blocs de 1024 octets
 */
res_block_list* rpc_read_1_svc(char** param, struct svc_req* req)
{
	check_auth(req);
	static res_block_list* result;
	if (!result) result = malloc(sizeof(res_block_list));
	block_list* curs = NULL;
	block_list* prev_curs = NULL;
	// Buffer temporaire pour la lecture
	char buf[BLOCKSIZE];
	// Fichier a lire
	FILE* fp = fopen(param[0], "r");
	// Taille lue
	size_t readLen;
	// Verifier qu'on a bien ouvert le fichier
	if (fp != NULL) {
		// Tant qu'il reste du contenu a lire dans le fichier	
		while ((readLen = fread(buf, sizeof(char), BLOCKSIZE-1, fp)) > 0) {
			// Terminer le buffer
			buf[readLen++] = '\0';
			curs = malloc(sizeof(block_list));
			curs->next = NULL;
			strcpy(curs->content, buf);
			if (prev_curs == NULL) {
				result->res_block_list_u.list = curs;
			} else {
				prev_curs->next = curs;
			}
			prev_curs = curs;
		}
		// Fermer le fichier apres la lecture
		fclose(fp);
	} else {
		// Si on a pas pu lire le fichier, renseigner
		// le code erreur
		result->error = errno;
		return result;
	}
	result->error = 0;
	return result;
}

int* rpc_write_1_svc(write_params* params, struct svc_req* req)
{
	check_auth(req);
	static int* result;
	result = malloc(sizeof(int));
	FILE* fichier; // Fichier a ouvrir
	char* mode; // Mode d'ouverture
	block_list* curs; // Curseur de parcours des donnees	
	// Choisir le mode d'ouverture en fonction des parametres
	mode = params->mode == 1 ? "w" : "a";
	fichier = fopen(params->name, mode);
	if (fichier == NULL) {
		// Si on a pas pu ouvrir le fichier
		// renvoyer un code erreur
		*result = errno;	
		return result;
	}
	// Parcourir les donnees passees en parametre
	curs = params->data;
	while (curs) {
		// Ecrire le contenu dans le fichier
		fwrite(curs->content, sizeof(char), strlen(curs->content), fichier);
		curs = curs->next;
	}
	fclose(fichier);
	*result = 0;
	return result;
}
