const MAXLEN = 255;
const BLOCKSIZE = 1024;
typedef string filename<MAXLEN>;

/* Liste de fichiers/repertoire  */
struct file_list {
	filename content; /* Nom de l'element */
	struct file_list* next; /* Adresse de la structure suivante */
};

/* Resultat d'un ls */
union res_file_list switch (int error) {
	case 0:
		file_list* list; /* Liste de fichier si il n'y a pas d'erreur */
	default:
		void; /* Ne rien renvoyer en cas d'erreur */
};

/* Liste de buffers de 1024 caracteres  */
struct block_list {
	char content[BLOCKSIZE]; /* Buffer */
	struct block_list* next; /* Adresse de la structure suivante */
};

/* Resultat d'un read  */
union res_block_list switch (int error) {
	case 0:
		block_list* list;
	default:
		void;
};

/* Parametres d'un write */
struct write_params {
	filename name; /* Nom du fichier */
	int mode; /* Mode d'ecriture */
	block_list* data; /* Donnees a ecrire */
};


program PROG {
	version PROG_VERS {
		res_file_list rpc_ls(string) = 1;
		res_block_list rpc_read(string) = 2;
		int rpc_write(write_params) = 3;
	} = 1;
} = 0x20000001;
