/**
 * TDM5 - Programme appelant
 * Envoie un nom de dossier a l'appele et
 * recoit une liste des fichiers dans ce
 * dossier
 */


#include "prog.h"
#include <stdio.h>
#include <errno.h>

// Taille maximum d'une entree utilisateur
#define MAX_INPUT_SIZE 255

/**
 * Affiche tous les elements d'une liste
 * chainee
 */
void print_linked_list(file_list* head) {
	file_list* curs = head;
	while (curs != NULL) {
		printf("%s\n", curs->content);
		curs = curs->next;
	}
}

/**
 * Affiche le contenu d'un fichier
 */
void print_file(block_list* head) {
	block_list* curs = head;
	while (curs != NULL) {
		printf("%s", curs->content);
		curs = curs->next;
	}
}

int main(int argc, char** argv)
{

	if (argc != 3) {
		fprintf(stderr, "Usage: %s HOST DIRECTORY\n", argv[0]);
		exit(-1);
	}

	CLIENT* client;
	// Adresse du serveur
	char* host = argv[1];
	// Liste des fichiers
	res_file_list* ls_result;
	// Fichier a ecrire
	char* new_file = "./new_file";
	// Fichier a lire
	char* read_file = "./test_file";
	// Repertoire a lister
	char* target = argv[2];
	// Resultat d'un write
	int* write_status; 
	// Resultat d'un read
	res_block_list* read_result; 

	// Parametres de modification du timeout
	struct timeval delai;
	delai.tv_sec = 60;
	delai.tv_usec = 0;

	// Connexion au serveur
	client = clnt_create(host, PROG, PROG_VERS, "tcp");
	if (client == NULL) {
		clnt_pcreateerror(argv[1]);
		exit(-2);
	}
	// Modification du timeout
	clnt_control(client, CLSET_TIMEOUT, (char*) &delai);
	// Mise en place du mode d'authentification	
	client->cl_auth = authunix_create_default();
	read_result = rpc_read_1(&target, client);
	if (read_result->error != 0) {
		puts("Lecture impossible");
		exit(-3);
	}
	// Lecture du repertoire demande
	ls_result = rpc_ls_1(&target, client);
	// Gestion du cas ou ls echoue
	if (ls_result->error != 0) {
		perror("Erreur ls");
		exit(-4);
	}
	// Affichage de la liste
	printf("Liste des fichiers: \n\n");
	print_linked_list(ls_result->res_file_list_u.list);
	// Liberation de l'espace alloue a la liste
	xdr_free((xdrproc_t) xdr_res_file_list, (char*) ls_result);
	// Lecture d'un fichier
	read_result = rpc_read_1(&read_file, client);
	if (read_result->error) {
		perror("Read");
		exit(-5);
	}
	printf("Contenu du fichier:\n\n");
	print_file(read_result->res_block_list_u.list);
	// Definition des parametres d'ecriture
	// Ici, on renvoie ce qu'on a lu
	write_params w_params = {
		.name = new_file,
		.mode = 1,
		.data = read_result->res_block_list_u.list
	};
	// Ecriture dans le nouveau fichier.
	write_status = rpc_write_1(&w_params, client);
	if (*write_status != 0) {
		perror("Write");
		exit(-6);
	}
	printf("\nEcriture reussie\n");
	xdr_free((xdrproc_t) xdr_res_block_list, (char*) read_result);
	return 0;
}
