/**
 * TDM3 - Programme serveur
 * Recoit une chaine de caracteres depuis le client
 * et l'ecrit dans /dev/null
 */


#include <rpc/rpc.h>
#include "exo1.h"

int* ecrire_1_svc(char ** param, struct svc_req *rqt)
{
	// Valeur de retour
	static int res;

	// Fichier dans lequel ecrire en mode append
	FILE* f = fopen("/dev/null", "a");
	// Si le fichier ne peut pas etre ouvert
	if (f == NULL) {
		// Retourner 1
		res = 1;
		return &res;
	}
	// Sinon ecrire la chaine passee en parametre dans le fichier 
	fprintf(f, "%s", *param);
	printf("Ecriture dans le fichier\n");
	// Fermer le fichier
	fclose(f);
	res = 0;
	return &res;
}
