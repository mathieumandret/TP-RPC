/**
 * TDM3 - Programme client
 * Prends une adresse en parametre et demande
 * l'ecriture dans /dev/null de la machine
 * a cette adresse d'une chaine de caracteres
 */


#include <rpc/rpc.h>
#include "exo1.h"

int main(int argc, char** argv)
{

	// Verification du bon nombre des paramètres
	if (argc != 2) {
		fprintf(stderr, "usage: %s HOST\n", argv[0]);
		exit(-1);
	}

	CLIENT* client;
	// Pointeur vers la valeur de retour
	int* result;
	// Chaine de caracteres a envoyer
	char* chaine = "A ecrire\n";

	// Connexion au serveur
	client = clnt_create(argv[1], ECRITURE_PROG, ECRITURE_VERS, "tcp");
	if (client == NULL) {
		// Si on a pas ete connectes, lever une erreur et quitter
		clnt_pcreateerror(argv[1]);
		exit(-1);
	}

	// Appel a la procedure
	result = ecrire_1(&chaine, client);
	
	if (result == NULL) {
		// Gestion d'une potentielle erreur au niveau de la procedure
		clnt_perror(client, argv[1]);
		exit(-2);
	}

	// Traiter le code retour
	if (*result == 0) {
		printf("Ecriture dans le fichier reussie\n");
	} else {
		printf("Erreur lors de l'ecriture dans le fichier\n");
	}
	// Le traitement a reussi au niveau du client
	return 0;
}
