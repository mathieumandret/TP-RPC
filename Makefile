CC=pandoc
dossier.pdf: dossier.pandoc
	$(CC) dossier.pandoc --template=eisvogel --toc --toc-depth=2 --highlight-style=pygments -o dossier.pdf

clean:
	rm *.pdf
