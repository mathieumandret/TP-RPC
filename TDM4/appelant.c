/**
 * TDM4 - Programme client
 * Prends en parametre une adresse et deux entiers et
 * fait calculer la somme de tous les entiers compris entre
 * ces derniers par le programme serveur a cette adresse
 */



#include <stdio.h>
#include <rpc/rpc.h>
#include "prog.h"

int main(int argc, char** argv)
{
	// Verification du nombre d'arguments
	if (argc != 4) {
		fprintf(stderr, "usage: %s HOST\n borne_inf borne_supp", argv[0]);
		exit(-1);
	}

	int inf = atoi(argv[2]);
	int supp = atoi(argv[3]);

	if (inf > supp) {
		fprintf(stderr, "La borne inferieure ne doit pas etre plus grande \
				que la superieure");
		exit(-2);
	}

	// Initialisation de la structure avec les parametre passes
	// au programme
	bornes params = {
		.inf = inf,
		.supp = supp
	};

	CLIENT* client;
	int* result;
	
	// Connexion au serveur
	client = clnt_create(argv[1], TEST_PROG, TEST_VERS, "tcp");
	if (client == NULL) {
		clnt_pcreateerror(argv[1]);
		exit(-3);
	}

	// Appel a la procedure
	result = somme_1(&params, client);

	if (result == NULL) {
		clnt_perror(client, argv[1]);
		exit(1);
	}

	printf("Somme des valeurs de %d a %d = %d\n",
			params.inf,
			params.supp,
			*result);
	exit(0);
}
