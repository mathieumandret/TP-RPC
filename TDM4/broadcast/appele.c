/**
 * TDM4 - Programme Serveur
 * Recoit une structure contenant 2 entiers et renvoie
 * la somme de toutes les entiers entre ses bornes (bornes comprises)
 */

#include "prog.h"

// Retourne la somme de tous les entiers de inf a supp
int sum(int inf, int supp) {
	if (inf == supp) {
		return inf;
	} else {
		return inf + sum(inf+1, supp);
	}
}

int* somme_1_svc(bornes* b, struct svc_req* rqt)
{
	// Somme
	static int res;
	res = sum(b->inf, b->supp);
	return &res;
}
