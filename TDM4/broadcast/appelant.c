/**
 * TDM4 - Programme client
 * Prends en parametre deux entiers et
 * fait calculer la somme de tous les entiers compris entre
 * ces derniers par tous les démons rpcbind du réseau
 */

#include <stdio.h>
#include <arpa/inet.h>
#include "prog.h"

/* Fonction appellee a chaque réponse d'un démon RPC */
int eachresult(int* res, struct sockaddr_in* addr) {
	// Adresse IP de la machine qui a répondu
	char adresse[INET_ADDRSTRLEN];		
	// Port de la machine
	unsigned int port;
	// Récuperation de l'adresse en string
	inet_ntop(addr->sin_family, &addr->sin_addr, adresse, INET_ADDRSTRLEN);
	// Recupération du port en entier
	port = ntohs(addr->sin_port);
	printf("Réponse de %s:%d:  %d\n", adresse, port, *res);
	return 1;
}

int main(int argc, char** argv)
{
	// Verification du nombre d'arguments
	if (argc != 3) {
		fprintf(stderr, "usage: %s borne_inf borne_supp\n", argv[0]);
		exit(-1);
	}

	int* err;
	int inf = atoi(argv[1]);
	int supp = atoi(argv[2]);
	if (inf > supp) {
		fprintf(stderr, "La borne inferieure ne doit pas etre plus grande que la superieure");
		exit(-2);
	}
	// Initialisation de la structure avec les parametre passes
	// au programme
	bornes params = {
		.inf = inf,
		.supp = supp
	};
	
	// Appel a la procedure
	err = somme_1(&params, eachresult);

	// Gestion de l'erreur
	if (err == NULL) {
		perror("Appel RPC broadcast");
		exit(-2);
	}
}
