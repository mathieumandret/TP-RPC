#include <memory.h> /* for memset */
#include <rpc/pmap_clnt.h>
#include "prog.h"


int *
somme_1(bornes *argp, bool_t (*eachresult)())
{
	static int clnt_res;

	memset((char *)&clnt_res, 0, sizeof(clnt_res));
	if (clnt_broadcast(TEST_PROG, TEST_VERS, somme,
		(xdrproc_t) xdr_bornes, (caddr_t) argp,
		(xdrproc_t) xdr_int, (caddr_t) &clnt_res,
		eachresult) != RPC_SUCCESS) {
		return (NULL);
	}
	return (&clnt_res);
}
