#ifndef _PROG_H_RPCGEN
#define _PROG_H_RPCGEN

#include <rpc/rpc.h>


#ifdef __cplusplus
extern "C" {
#endif


struct bornes {
	int inf;
	int supp;
};
typedef struct bornes bornes;

#define TEST_PROG 0x20000002
#define TEST_VERS 1

#if defined(__STDC__) || defined(__cplusplus)
#define somme 1
extern  int * somme_1(bornes *, bool_t (*eachresult)());
extern  int * somme_1_svc(bornes *, struct svc_req *);
extern int test_prog_1_freeresult (SVCXPRT *, xdrproc_t, caddr_t);

#else /* K&R C */
#define somme 1
extern  int * somme_1();
extern  int * somme_1_svc();
extern int test_prog_1_freeresult ();
#endif /* K&R C */

/* the xdr functions */

#if defined(__STDC__) || defined(__cplusplus)
extern  bool_t xdr_bornes (XDR *, bornes*);

#else /* K&R C */
extern bool_t xdr_bornes ();

#endif /* K&R C */

#ifdef __cplusplus
}
#endif

#endif /* !_PROG_H_RPCGEN */
