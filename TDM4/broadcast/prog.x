program TEST_PROG {
	version TEST_VERS {
		int somme(bornes) = 1;
	} = 1;
} = 0x20000002;

struct bornes {
	int inf;
	int supp;
};
